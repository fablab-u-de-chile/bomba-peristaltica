# Bomba peristáltica

Una pequeña bomba peristáltica impresa en 3D, funciona mediante un impulsor de rodamientos que comprime una manguera, empujando algún fluido viscoso. Hasta ahora se ha probado exitosamente con glicerina. Incluye programas de Arduino con control en lazo abierto y lazo cerrado-

<img src="/img/isobomb.png" width="400">

## Atributos

- Flujo de x cc/s.
- Manguera de silicona de 6 mm
- Actuado por motor DC.

[VIDEO POR GRABAR](www.google.com) del sistema funcionando.

## Cómo construirlo

- [LISTADO](parts.md) de partes, piezas y herramientas. 
- [Partes CAD](parts/).
- [ELECTRÓNICA](elec.md).
- Instrucciones de ensamble.

## Trabajo futuro

- Disminuir el efecto de chorro pulsante a la salida

### Ponte en contacto

Este actuador es parte de un [proyecto](https://gitlab.com/fablab-u-de-chile/biomixer) más grande desarrollado por el FabLab U de Chile. Puedes ver más de nuestra labor en:

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

## Licencia

Este trabajo está licenciado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png




