# Electrónica

Dado que la bomba consta de un motor DC pequeño, la forma tradicional de utilizarlo es mediante un puente H y algún controlador, en este caso lo implementamos como aparece en el diagrama más abajo.

<img src="/img/elec.png" width="600">

## Control

El sistema puede ser operado en lazo abierto o lazo cerrado. Las conexiones dependerán de los pines asignados en el programa, los cuales se encuentran en [src](/src).

### Lazo abierto

Para el control de lazo abierto basta el circuito anterior y pruebas que relacionen algún par de variables como el flujo volumétrico del fluido a utilizar respecto al voltaje, y luego controlar el dispensado con un temporizador.

### Lazo cerrado

Para un dispensado más preciso es posible utilizar una [celda de carga](https://www.instructables.com/Arduino-Scale-With-5kg-Load-Cell-and-HX711-Amplifi/) para retroalimentar al controlador para generar un sistema más eficiente, como muestra el siguiente diagrama:

<img src="/img/elec2.png" width="600">

Para utilizar la celda es necesario descargar la librería HX711 de Arduino. El programa que se adjunta utiliza por defecto una celda de 1kg y un controlador proporcional. Considerando que es más común dispensar fluidos en cantidades volumétricas, es necesario disponder del volúmen específico de la sustancia para que tenga sentido el uso de la celda de carga, que mide masa.

## Listado de componentes

 ITEM              | Cantidad
 ---------------------------   | ------------
Arduino UNO | 1
Puente H L298N | 1
Celda de carga 1 kg | 1
Amplificador HX711 | 1

+ Cables de conexión.



