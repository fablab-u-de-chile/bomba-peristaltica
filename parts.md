# Partes y piezas

## Partes comerciales

 ITEM              | Cantidad
 ---------------------------   | ------------
 Motor DC 12V 60RPM | 1
 Perno cabeza plana M5x30 | 4
 Perno cabeza plana M5x15 | 3
 Tuerca  M5 de bajo perfil | 3
 Tuerca M5 | 4
 Rodamiento 605ZZ | 6
 M3x8 perno avellanado | 2
 Tuerca M3 | 3
 Prisionero M3x6 | 3
 Manguera de silicona 6 mm DE | 30 cm min.
 Golilla M4 | 1 

## Partes impresas

Los archivos .step y .stl los puedes encontrar en la [carpeta de partes](parts/).

 ITEM                   | Cantidad
 ---------------------------   | ------------
 Tapa superior| 1
 Tapa inferior | 1
 Caja bomba | 1
 Impulsor | 1

## Herramientas

 ITEM |                  
 --------------------------- |  
 Tijera |
 Llave allen 4 mm |
 Llave allen 3 mm |
 Llave allen 2 mm |
 Llave allen 1.5 mm |



